package ru.dyatchina.tm.entity;

import ru.dyatchina.tm.enumerated.Role;

public class User {

    private Long id = System.nanoTime();

    private String login;

    private String passwordHash;

    private String fistName = "";

    private String lastName = "";

    private String middleName = "";

    private Role role = Role.USER;

    public Role getRole() {
        return role;    }

    public void setRole(Role role) {this.role = role;}

    public String getFistName() {return fistName;}

    public User(String login, String passwordHash, Role role) {
        this.login = login;
        this. passwordHash = passwordHash;
        this.role = role;
    }

    public User(String login, String passwordHash) {
        this.login = login;
        this. passwordHash = passwordHash;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
}
