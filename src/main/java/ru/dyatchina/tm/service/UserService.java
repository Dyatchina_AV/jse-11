package ru.dyatchina.tm.service;

import ru.dyatchina.tm.entity.User;
import ru.dyatchina.tm.enumerated.Role;
import ru.dyatchina.tm.repository.UserRepository;
import ru.dyatchina.tm.util.HashUtil;

import java.util.List;

public class UserService {

    private final UserRepository userRepository;

    public UserService(final UserRepository userRepository) {this.userRepository = userRepository;}

    public User create(final String login, final String password) {
        if ((login == null  || login.isEmpty()) || (password == null || password.isEmpty()) || (existsByLogin(login))) return null;
        final String passwordHash = HashUtil.md5(password);
        final User user = new User(login, passwordHash);
        userRepository.add(user);
        return user;
    }

    public User create(final String login, final String password, final Role role) {
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    public void clear() {userRepository.clear();}

    public User update(final Long id, final String firstName, final String lastName, final String middleName) {
        final User user = findById(id);
        if (user == null) return null;
        user.setFistName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }
    public User updateByLogin(final String login, final String firstName, final String lastName, final String middleName) {
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setFistName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    public User findByIndex(final Integer index) {
        if (index == null) return null;
        return userRepository.findByIndex(index);
    }

    public User add(final User user) {
        if (user == null) return null;
        return userRepository.add(user);
    }

    public List<User> findAll() {return userRepository.findAll();}

    public User findById(final Long id) {
        if (id == null) return null;
        return userRepository.findById(id);
    }

    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    public User removeById(final Long id) {
        if (id == null) return null;
        return userRepository.removeById(id);
    }

    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.removeByLogin(login);
    }

    public User remove(final User user) {
        if (user == null) return null;
        return userRepository.remove(user);
    }

    public boolean existsByLogin(final String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.existsByLogin(login);
    }

    public boolean existsById(final Long id) {
        if (id == null) return false;
        return userRepository.existsById(id);
    }


}
