package ru.dyatchina.tm.controller;

import ru.dyatchina.tm.entity.Task;
import ru.dyatchina.tm.entity.User;
import ru.dyatchina.tm.enumerated.Role;
import ru.dyatchina.tm.repository.UserRepository;
import ru.dyatchina.tm.service.UserService;

import java.util.List;

public class UserController extends AbstractConroller{

    private final UserRepository userRepository = new UserRepository();
    private final UserService userService  = new UserService(userRepository);

    public UserController(UserService userService) {
    }

    public int createUser(final String login, final String password, Role role) {
        userService.create(login, password, role);
        return 0;
    }

    public int createUser() {
        System.out.println("[CREATE USER]");
        System.out.println("PLEASE, ENTER LOGIN: ");
        final String login = scanner.nextLine();
        if (login == null || login.isEmpty()) {
            System.out.println("[FAIL]: ");
            System.out.println("LOGIN CANNOT BE EMPTY: ");
            return 0;
        }
        if (userRepository.findByLogin(login) != null) {
            System.out.println("[FAIL]: ");
            System.out.println("A USER WITH SUCH LOGIN ALREADY EXISTS: ");
            return 0;
        }
        System.out.println("PLEASE, ENTER PASSWORD: ");
        final String password = scanner.nextLine();
        if (password == null || password.isEmpty()) {
            System.out.println("[FAIL]: ");
            System.out.println("PASSWORD CANNOT BE EMPTY: ");
            return 0;
        }
        userService.create(login, password);
        System.out.println("[OK]");
        return 0;
    }

    public int listUser() {
        System.out.println("[LIST USER]");
        userRepository.findAll().forEach(user -> System.out.println(user.toString()));
        System.out.println("[OK]");
        return 0;
    }

    public int viewUserById() {
        return  0;
    }

    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[VIEW USER]");
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("PASSWORD: " + user.getPasswordHash());
        System.out.println("ROLE: " + user.getRole());
        System.out.println("[OK]");
    }

    public int viewUserByLogin() {
        return 0;
    }

    public int viewUserByIndex() {
        return 0;
    }

    public int updateUserById() {
        return 0;
    }

    public int updateUserByLogin() {
        System.out.println("[UPDATE USER]");
        System.out.println("PLEASE, ENTER TASK INDEX: ");
        final String login = scanner.nextLine();
        if (login == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[PLEASE, ENTER FIRSTNAME]");
        final String firstName = scanner.nextLine();
        System.out.println("[PLEASE, ENTER LASTNAME]");
        final String lastName = scanner.nextLine();
        System.out.println("[PLEASE, ENTER MIDDLEMANE]");
        final String middleName = scanner.nextLine();
        userService.updateByLogin(login, firstName, lastName, middleName);
        System.out.println("[OK]");
        return 0;
    }

    public int updateUserByIndex() {
        return 0;
    }

    public int removeUserById() {
        return 0;
    }

    public int removeUserByLogin() {
        System.out.println("[REMOVE USER]");
        System.out.println("PLEASE, ENTER LOGIN: ");
        final String login = scanner.nextLine();
        userService.removeByLogin(login);
        if (login == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    public int removeUserByIndex() {
        return 0;
    }

    public int clear() {
        return 0;
    }


}
