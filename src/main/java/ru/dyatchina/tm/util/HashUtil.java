package ru.dyatchina.tm.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashUtil {

    public static String md5(final String value) {
        try {
            final MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(value.getBytes(),0,value.length());
            return new BigInteger(1, md.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }

    }

}
