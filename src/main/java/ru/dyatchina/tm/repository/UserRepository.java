package ru.dyatchina.tm.repository;

import ru.dyatchina.tm.entity.User;
import ru.dyatchina.tm.enumerated.Role;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private List<User> users = new ArrayList<>();

    public User create(final String login, final String password) {
        User user = new User(login, password);
        user.setRole(Role.USER);
        users.add(user);
        return user;
    }

    public User create(final String login, final String password, final Role role) {
        User user = new User(login, password, role);
        users.add(user);
        return user;
    }

   public User add(final User user) {
        users.add(user);
        return user;
    }

    public List<User> findAll() {
        return users;
    }

    public boolean existsByLogin(final String login) {
        final User user = findByLogin(login);
        return user != null;
    }

    public boolean existsById(final Long id) {
        final User user = findById(id);
        return user != null;
    }

    public User findByIndex(final Integer index) {
        if (index < 0 || index > users.size() -1) return null;
        return users.get(index);
    }

    public User findById(final Long id) {
        for (final User user: users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    public User findByLogin(final String login) {
        for (final User user: users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    public User removeById(final Long id) {
        final User user = findById(id);
        if (user == null) return null;
        remove(user);
        return user;
    }

    public User removeByLogin(final String login) {
       for (final User user: users) {
           if (login.equals(user.getLogin())) {
               users.remove(user);
               return user;
           }
       }
        return null;
    }

    public User removeByIndex(final Integer index) {
        final User user = findByIndex(index);
        if (user == null) return null;
        remove(user);
        return user;
    }

    public User remove(final User user) {
        users.remove(user);
        return user;
    }

    public void clear() {users.clear();}
}
