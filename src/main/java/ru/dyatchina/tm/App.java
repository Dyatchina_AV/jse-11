package ru.dyatchina.tm;

import ru.dyatchina.tm.controller.ProjectController;
import ru.dyatchina.tm.controller.SystemController;
import ru.dyatchina.tm.controller.TaskController;
import ru.dyatchina.tm.controller.UserController;
import ru.dyatchina.tm.entity.User;
import ru.dyatchina.tm.enumerated.Role;
import ru.dyatchina.tm.repository.ProjectRepository;
import ru.dyatchina.tm.repository.TaskRepository;
import ru.dyatchina.tm.repository.UserRepository;
import ru.dyatchina.tm.service.ProjectService;
import ru.dyatchina.tm.service.ProjectTaskService;
import ru.dyatchina.tm.service.TaskService;
import ru.dyatchina.tm.service.UserService;

import java.util.Scanner;

import static ru.dyatchina.tm.constant.TerminalConst.*;

/*
*Учебное приложение "Таск-менеджер"
*
*/


public class App {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskRepository taskRepository = new TaskRepository();

    private final TaskService taskService = new TaskService(taskRepository);

    private final UserRepository userRepository = new UserRepository();

    private final UserService userService = new UserService(userRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskController taskController = new TaskController(taskService, projectTaskService);

    private final SystemController systemController = new SystemController();

    private final UserController userController = new UserController(userService);

    {
        projectRepository.create("DEMO PROJECT 1");
        projectRepository.create("DEMO PROJECT 2");
        taskRepository.create("TEST TASK 1");
        taskRepository.create("TEST TASK 2");
        userService.create("test", "test");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public static void main(final String[] args) {
         final Scanner scanner = new Scanner(System.in);
        final App app = new App();
        app.run(args);
        app.systemController.displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            app.run(command);
        }
    }

    public void run(final String[] args){
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
        }


    public int run(final String param){
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case VERSION: return systemController.displayVersion();
            case ABOUT: return  systemController.displayAbout();
            case HELP: return systemController.displayHelp();
            case EXIT: return systemController.displayExit();

            case PROJECT_LIST: return projectController.listProject();
            case PROJECT_CLEAR: return projectController.clearProject();
            case PROJECT_CREATE: return projectController.createProject();
            case PROJECT_VIEW: return projectController.viewProjectByIndex();
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();

            case TASK_CLEAR: return taskController.clearTask();
            case TASK_CREATE: return taskController.createTask();
            case TASK_LIST: return taskController.listTask();
            case TASK_VIEW: return taskController.viewTaskByIndex();
            case TASK_REMOVE_BY_NAME: return taskController.removeTaskByName();
            case TASK_REMOVE_BY_ID: return taskController.removeTaskById();
            case TASK_REMOVE_BY_INDEX: return taskController.removeTaskByIndex();
            case TASK_UPDATE_BY_INDEX: return taskController.updateTaskByIndex();
            case TASK_ADD_TO_PROJECT_BY_IDS: return taskController.addTaskToProjectById();
            case TASK_LIST_BY_PROJECT_ID: return taskController.listTaskByProjectId();
            case TASK_REMOVE_FROM_PROJECT_BY_IDS: return taskController.removeTaskFromProjectById();

            case USER_CREATE: return userController.createUser();
            case USER_LIST: return userController.listUser();
            case USER_VIEW_BY_ID: return userController.viewUserById();
            case USER_VIEW_BY_LOGIN: return userController.viewUserByLogin();
            case USER_VIEW_BY_INDEX: return userController.viewUserByIndex();
            case USER_UPDATE_BY_ID: return userController.updateUserById();
            case USER_UPDATE_BY_LOGIN: return userController.updateUserByLogin();
            case USER_UPDATE_BY_INDEX: return userController.updateUserByIndex();
            case USER_REMOVE_BY_ID: return userController.removeUserById();
            case USER_REMOVE_BY_LOGIN: return userController.removeUserByLogin();
            case USER_REMOVE_BY_INDEX: return userController.removeUserByIndex();

            case USER_CLEAR: return userController.clear();

            default: return systemController.displayError();
        }
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }
}
